<?php
class Article extends Model
{
  public function __construct()
  {
    $this->table = 'articles';
    $this->acronym = 'ART';
    $this->interface = 'articles';
    $this->getConnexion();
  }
}
