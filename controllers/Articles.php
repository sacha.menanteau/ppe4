<?php
class Articles extends Controllers
{
  public function index()
  {
    $this->loadModel("Article");
    $articles = $this->Article->getAll();
    $this->render('index', compact('articles'));
  }

  public function lire($id){
    $this->loadModel('Article');
    $this->id = $id;
    $article = $this->Article->getOne();
    $this->render('lire', compact('article'));
  }
}
