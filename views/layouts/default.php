    <header>
      <div class="navbar">
        <ul>
          <li>
            <a href="accueil">
              <h2>
                <i class="fa fa-globe"></i>
                <em>PPE 4</em>
              </h2>
            </a>
          </li>
          <li>
            <a href="articles">
              <h2><em>Articles</em></h2>
            </a>
          </li>
        </ul>
      </div>
    </header>
    <main>
      <?= $content ?>
    </main>
    <footer>
      <div class="footer-content">
        <p>Bas de page</p>
      </div>
    </footer>
  </body>
</html>
