<h1><em>Articles</em></h1>
<div id="list-articles">
  <?php foreach ($articles as $narticle => $article): ?>
  <a href="articles/lire/<?= $article['ART_ID'] ?>" class="text-link">
    <h2><?= $article['ART_TITRE'] ?></h2>
  </a>
  <p><?= $article['ART_CONTENT'] ?></p>
  <?php endforeach; ?>
</div>
