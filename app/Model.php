<?php
abstract class Model
{
  // DB informations
  private $host = "localhost";
  private $db   = "ppe4";
  private $user = 'root';
  private $pwd  = '';

  // Propriétés connexion
  protected $_connexion;
  // Propriétés infos requêtes
  public $table;
  public $id;
  public $acronym;

  public function getConnexion(){
    $this -> _connexion = null;
    try
    {
      $this->_connexion = new PDO('mysql:host='.$this->host.'; dbname='.$this->db, $this->user, $this->pwd);
      $this->_connexion->exec('set names utf8');
    }
    catch (PDOException $exception)
    {
      echo 'Erreur: '.$exception->getMessage();
    }
  }

  public function getAll()
  {
    $sql = "SELECT * FROM ".$this->table;
    $query = $this->_connexion->prepare($sql);
    $query->execute();
    return $query->fetchAll();
  }

  public function getOne()
  {
    $sql = "SELECT * FROM ".$this->table." WHERE ".$this->acronym."_ID = '".$this->id."'";
    $query = $this->_connexion->prepare($sql);
    $query->execute();
    return $query->fetch();
  }
}
