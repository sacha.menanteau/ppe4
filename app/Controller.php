<?php
abstract class Controllers{
  public function loadModel(string $model)
  {
    require_once(ROOT.'models/'.$model.'.php');
    $this -> $model = new $model();
  }
  public function render(string $fichier, array $datas = [])
  {
    extract($datas);
    ob_start();
    require_once(ROOT.'views/layouts/links.php');
    require_once(ROOT.'views/'.strtolower(get_class($this)).'/'.$fichier.'.php');
    $content = ob_get_clean();
    require_once(ROOT.'views/layouts/default.php');
  }
}
